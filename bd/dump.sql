-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 18, 2019 at 11:36 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Testing_fullstack`
--

-- --------------------------------------------------------

--
-- Table structure for table `Construccion`
--

CREATE TABLE `Construccion` (
  `ID` int(11) NOT NULL,
  `Nombre` varchar(255) NOT NULL,
  `Calle` varchar(255) NOT NULL,
  `Numero` varchar(10) NOT NULL,
  `Colonia` varchar(255) NOT NULL,
  `Delegacion` varchar(255) NOT NULL,
  `Latitud` decimal(11,8) NOT NULL,
  `Longitud` decimal(11,8) NOT NULL,
  `Clave` varchar(11) NOT NULL,
  `IdUsr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Construccion`
--

INSERT INTO `Construccion` (`ID`, `Nombre`, `Calle`, `Numero`, `Colonia`, `Delegacion`, `Latitud`, `Longitud`, `Clave`, `IdUsr`) VALUES
(1, 'Prueba', 'Reforma', '123', 'Reforma', 'Miguel HIdalgo', '23.56456000', '-12.34534535', 'PCOM-ABC/01', 3),
(2, 'Prueba 2 extras', 'Reforma', '123', 'Reforma', 'GUSTAVO A MADERO', '28.56458000', '-12.34534535', 'PCOM-ABC/02', 3),
(3, 'Prueba 3 editada', 'Insurgentes', '123', 'insurgentes', 'Benito Juárez', '34.45674568', '-42.56756757', 'PCOM-ABC/03', 3),
(4, 'Prueba 4', 'asd', '232', 'Son gfgd', 'GUSTAVO A MADERO', '23.56456000', '-12.34534535', 'PCOM-ABC/04', 3),
(5, 'Construcción otro usuario', 'Calzada de Talpan', '5252', 'Tlalpan', 'Tlalpan', '56.23423400', '-34.76576760', 'PCOM-ABC/07', 4);

-- --------------------------------------------------------

--
-- Table structure for table `Extras_construccion`
--

CREATE TABLE `Extras_construccion` (
  `ID` int(11) NOT NULL,
  `id_construccion` int(11) NOT NULL,
  `caracteristica` varchar(255) NOT NULL,
  `valor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Extras_construccion`
--

INSERT INTO `Extras_construccion` (`ID`, `id_construccion`, `caracteristica`, `valor`) VALUES
(5, 4, 'color', 'verde'),
(6, 2, 'Color', 'Rosa'),
(7, 2, 'Altura', '27'),
(8, 2, 'pisos', '4'),
(11, 5, 'color', 'azul');

-- --------------------------------------------------------

--
-- Table structure for table `galeria`
--

CREATE TABLE `galeria` (
  `ID` int(11) NOT NULL,
  `id_construccion` int(11) NOT NULL,
  `foto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `ID` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`ID`, `FirstName`, `LastName`, `Email`, `Password`) VALUES
(1, 'Nathan', 'Smith', 'pcom@gmail.com', '3dd14afc9f2da6c03c4f6599553a4597'),
(3, 'Gibrán', 'Vázquez', 'vazquez.gbrn@gmail.com', 'd6558b0be179868cb54e2096d37644b1df0bf405'),
(4, 'Usuario', 'Otro Correo', 'gibran@vazquez.com', 'd6558b0be179868cb54e2096d37644b1df0bf405');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Construccion`
--
ALTER TABLE `Construccion`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IdUsr` (`IdUsr`);

--
-- Indexes for table `Extras_construccion`
--
ALTER TABLE `Extras_construccion`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `id_construccion` (`id_construccion`);

--
-- Indexes for table `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `id_construccion` (`id_construccion`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Construccion`
--
ALTER TABLE `Construccion`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `Extras_construccion`
--
ALTER TABLE `Extras_construccion`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `galeria`
--
ALTER TABLE `galeria`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Construccion`
--
ALTER TABLE `Construccion`
  ADD CONSTRAINT `construccion_ibfk_1` FOREIGN KEY (`IdUsr`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Extras_construccion`
--
ALTER TABLE `Extras_construccion`
  ADD CONSTRAINT `extras_construccion_ibfk_1` FOREIGN KEY (`id_construccion`) REFERENCES `Construccion` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `galeria`
--
ALTER TABLE `galeria`
  ADD CONSTRAINT `galeria_ibfk_1` FOREIGN KEY (`id_construccion`) REFERENCES `Construccion` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
