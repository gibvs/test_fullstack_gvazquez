
    <style>
      #map {
        height: 400px; 
        width: 100%;  
       }
    </style>
  </head>
  <body>
    <div class="container">
    <h3>Construcciones</h3>
    <a href="<?= base_url("login") ?>">Iniciar Sesión</a>
    
    <div class="lista row">

        <div class="head_const col-md-12">
            <div class="col-md-3">Construcción</div>
            <div class="col-md-3">Dirección</div>
            <div class="col-md-3">Usuario</div>
            <div class="col-md-3">Detalles</div>
        </div>
        <?php foreach ($const as $cst) {
        ?>
            <div class="construccion col-md-12">
                <div class="nombre_cst col-md-3"><?= $cst->Nombre ?></div>
                <div class="direccion col-md-3"><?php echo $cst->Calle." ".$cst->Numero." ".$cst->Colonia." ".$cst->Delegacion; ?></div>
                <div class="direccion col-md-3"><?php echo $cst->usuario; ?></div>
                <div class="detalles col-md-3"><button class="btn-info btn" class="detalles" data-idcst="<?= $cst->ID ?>" data-toggle="modal" data-target="#detalles">Ver detalles</button></div>
            </div>
        <?php
        }
        ?>
    </div>

    <div id="map"></div>

    <script>
// Initialize and add the map
function initMap() {
  var uluru = {lat: -25.344, lng: 131.036};
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 4, center: uluru});
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
    </script>
  </div>
  </body>

