
<div class="container">
    <h3>Bienvenido <?= $this->session->userdata("nombre") ?></h3>
    <a href="<?= base_url("login/cerrar") ?>">Cerrar Sesión</a>
    
    <div class="lista row">

      <?php if ($this->session->flashdata('registro')) { ?>
                <div class="alert alert-success" role="alert">
                    Se registró la construcción.
                </div>
            <?php } ?>
        <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#agregar">
    Agregar Construcción
</button>

        <div class="head_const col-md-12">
            <div class="col-md-4">Construcción</div>
            <div class="col-md-4">Dirección</div>
            <div class="col-md-4">Detalles</div>
        </div>
        <?php foreach ($const as $cst) {
        ?>
            <div class="construccion col-md-12">
                <div class="nombre_cst col-md-4"><?= $cst->Nombre ?></div>
                <div class="direccion col-md-4"><?php echo $cst->Calle." ".$cst->Numero." ".$cst->Colonia." ".$cst->Delegacion; ?></div>
                <div class="detalles col-md-4"><button class="btn-info btn" class="detalles" data-idcst="<?= $cst->ID ?>" data-toggle="modal" data-target="#agregar">Editar</button></div>
            </div>
        <?php
        }
        ?>
    </div>
</div>

