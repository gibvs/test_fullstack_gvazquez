<div class="container">
    <div class="row text-center">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4">
            <br>
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger" role="alert">
                    Lo sentimos, ya existe una cuenta con ese correo electrónico.
                </div>
            <?php } ?>
            <br>
            <form role="form" id="nv_usr" action="nuevo_usr/registrar" method="post">
                <h3>Ingrese los datos solicitados para registrarse en la plataforma</h3>
                <div>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                    <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido">
                    <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo electrónico">
                    <input id="password" name="password" type="password" class="form-control" placeholder="Contraseña">
                    <input id="confirm_password" type="password" class="form-control" placeholder="Confirmar contraseña">
                </div>
            </form>
            
                <center><button  id="login" class="btn btn-success ">Registrar</button></center>
        </div>
    </div>
</div><!-- /container -->