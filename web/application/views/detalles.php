<div class="modal fade" id="detalles" tabindex="-1" role="dialog" aria-labelledby="detallesLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detallesLabelTtl">Detalles de construccion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
         <div class="modal-body">
         
                        <div class="row">
                          <div class="col-md-6 nom">Nombre</div><div class="col-md-6 val" id="nombre"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 nom">Clave</div><div class="col-md-6 val" id="clave"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 nom">Calle</div><div class="col-md-6 val" id="calle"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 nom">Número</div><div class="col-md-6 val" id="numero"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 nom">Colonia</div><div class="col-md-6 val" id="colonia"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 nom">Delegación</div><div class="col-md-6 val" id="delegacion"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 nom">Latitud</div><div class="col-md-6 val" id="latitud"></div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 nom">Longitud</div><div class="col-md-6 val" id="longitud"></div>
                        </div>
                           


                    <div>Datos extra</div>
                    <div class="row" id="extras">
                    </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
    </div>
  </div>
</div>