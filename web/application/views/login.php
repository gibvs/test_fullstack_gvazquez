<div class="container">
    <div class="row text-center">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4">
            <br>
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger" role="alert">
                    Usuario o Contraña incorrecta.
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('registro')) { ?>
                <div class="alert alert-success" role="alert">
                    Su registro fue exitoso, ahora puede iniciar sesión con su correo y contraseña. 
                </div>
            <?php } ?>
            <br>
            <form role="form" id="form_id" action="login/validar" class="form-signin " method="post">
                <div class="row">
                    <input type="text" class="form-control" id="correo" name="correo" placeholder="Ingrese su Correo Electrónico">
                    <input id="login-form_input--password" name="password" type="password" class="form-control" placeholder="Contraseña">
                </div>
                <div class="row">
                    <center><button  id="login" class="btn btn-success " type="submit" >Enviar</button></center>
                </div>

                <div class="row">
                    <a href="<?= base_url('nuevo_usr') ?>">Registrar nuevo usuario</a>
                </div>
            </form>
        </div>
    </div>
</div><!-- /container -->