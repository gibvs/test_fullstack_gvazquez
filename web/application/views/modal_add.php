<div class="modal fade" id="agregar" tabindex="-1" role="dialog" aria-labelledby="agregarLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="agregarLabelTtl">Agregar construccion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
         <div class="modal-body">
                    <div>Ingrese los datos solicitados para una nueva construcción</div>
                    <form role="form" id="const_form" action="usuarios/agregar_cst" method="post">
         
                        <div>
                        	<div>
                        		<div class="col-xs-12">
                        		<label>Nombre:</label>
                        		</div>
                            	<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                            </div>
                        	<div>
                        		<div class="col-xs-12">
	                        		<label>Clave:</label>
	                        	</div>
                            	<div class="col-xs-3">
                            		<input type="text" class="form-control" value="PCOM" readonly="readonly">
                            	</div>
                            	<div class="col-xs-1">
                            		-
                            	</div>
                            	<div class="col-xs-3">
                            		<input type="text" class="form-control" maxlength="3" id="str" name="str" placeholder="XXX">
                            	</div>
                            	<div class="col-xs-1">
                            		/
                            	</div>

                            	<div class="col-xs-3">
                            		<input type="text" class="form-control" maxlength="2" id="num" name="num" placeholder="##">
                            	</div>
                            </div>
                        	<div>
                        		<div class="col-xs-12">
                        			<label>Calle:</label>
                        		</div>
	                            <input type="text" class="form-control" id="calle" name="calle" placeholder="Calle">
	                        </div>
                        	<div>
                        		<div class="col-xs-12">
                        			<label>Número:</label>
                        		</div>
                        			<input type="text" class="form-control" id="numero" name="numero" placeholder="Numero"> 
                        	</div>
                        	<div>
                        		<div class="col-xs-12">
                        			<label>Colonia:</label>
                        		</div>
                            	<input type="text" class="form-control" id="colonia" name="colonia" placeholder="Colonia">
                            </div>
                        	<div>
                        		<div class="col-xs-12">
                        			<label>Delegación:</label>
                        		</div>
                            	<input type="text" class="form-control" id="delegacion" name="delegacion" placeholder="Delegación">
                            </div>
                        	<div>
                        		<div class="col-xs-12">
                        			<label>Latitud:</label>
                        		</div>
                            	<input type="text" class="form-control" id="latitud" name="latitud" placeholder="Latitud">
                            </div>
                        	<div>
                        		<div class="col-xs-12">
                        			<label>Longitud:</label>
                        		</div>
	                            <input type="text" class="form-control" id="longitud" name="longitud" placeholder="Longitud">
	                        </div>
                        </div>


                    <div>Si desea agregar más detalle, haga click en el botón aquí</div>
                    <button type="button" class="btn btn-primary" id="agregar_car">
                        Agregar característica
                    </button>
                    <div class="row">
                        <div class='hd'><div class='col-md-6'>Característica</div><div class='col-md-6'>Valor</div></div>
                        <div id="extras"></div>
                    </div>

                            <input type="hidden" id="clave" name="clave" value="0">
                            <input type="hidden" id="idct" name="idct" value="0">
                    </form>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" id="guarda_cst">Guardar</button>
          </div>
    </div>
  </div>
</div>