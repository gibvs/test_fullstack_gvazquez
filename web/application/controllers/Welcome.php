<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->model("construccion_md");
		$const=$this->construccion_md->getView();

		$params['const']=$const;
		$footer['custom_js']=array("home.js");
		$this->load->view('header');
		$this->load->view('lista',$params);
		$this->load->view('detalles');
		$this->load->view('footer',$footer);
	}
}
