<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	
	 function __construct(){
		 
        parent::__construct();
		$this->load->model('users_md');
		
	}

	public function index(){		
		
		$footer['custom_js']=array();
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer',$footer);
	}
		
	public function validar(){
		
		$email=filter_var($this->input->post('correo'), FILTER_SANITIZE_EMAIL);
		$pass=$this->input->post('password');
		
		$usr=$this->users_md->isUs($email,sha1($pass));
		
		$redirect="login";
		if( count($usr)>0){
			$usr=$usr[0];
			$usrdt=array(
					'id'=>$usr->ID,
					'nombre'=>$usr->FirstName." ".$usr->LastName
				);
			$this->session->set_userdata($usrdt);
			$redirect='usuarios';
		}
		else{
			$this->session->set_flashdata("error","El usuario o la contraseña no son correctos.");
		}
		//echo $redirect;
		redirect(base_url($redirect));
	}
	
		
	public function cerrar(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}