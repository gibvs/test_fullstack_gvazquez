<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->load->model('construccion_md');
        $this->load->model('extras_construccion_md');

    }


	public function index()
	{
        $this->checkSes();
		$this->load->model("construccion_md");
		$const=$this->construccion_md->getByUsr($this->session->userdata("id"));

		$params['const']=$const;
		$footer['custom_js']=array("usr.js");

		$this->load->view('header');
		$this->load->view('mis_cst',$params);
		$this->load->view('modal_add');
		$this->load->view('footer',$footer);
	}

	function agregar_cst(){
        $this->checkSes();
		$data=array();

		array_push($data,filter_var($this->input->post('nombre'), FILTER_SANITIZE_STRING));
		array_push($data,filter_var($this->input->post('calle'), FILTER_SANITIZE_STRING));
		array_push($data,filter_var($this->input->post('numero'), FILTER_SANITIZE_STRING));
		array_push($data,filter_var($this->input->post('colonia'), FILTER_SANITIZE_STRING));
		array_push($data,filter_var($this->input->post('delegacion'), FILTER_SANITIZE_STRING));
		array_push($data,filter_var($this->input->post('latitud'), FILTER_SANITIZE_STRING));
		array_push($data,filter_var($this->input->post('longitud'), FILTER_SANITIZE_STRING));
		array_push($data,filter_var($this->input->post('clave'), FILTER_SANITIZE_STRING));
		array_push($data,$this->session->userdata("id"));

		$idct=$this->input->post("idct");

		if($idct==0){
			$id=$this->construccion_md->insertRecord($data);
		}
		else{
			$id=$this->construccion_md->updateRecord($data,$idct);
			$this->extras_construccion_md->delCt($idct);
		}

		$extras=array();
		foreach ($_POST as $key => $value) {
			if(strpos($key,'car_') !== false){
				$idx=substr($key,4,strlen($key));
				if($this->input->post('car_'.$idx)!=""&&$this->input->post('vl_'.$idx)!=""){
					$in_ar=array();
					array_push($in_ar,$id);
					array_push($in_ar,filter_var($this->input->post('car_'.$idx), FILTER_SANITIZE_STRING));
					array_push($in_ar,filter_var($this->input->post('vl_'.$idx), FILTER_SANITIZE_STRING));

					array_push($extras, $in_ar);
				}
			
			}
		}

		foreach ($extras as $extra) {
			$this->extras_construccion_md->insertRecord($extra);
		}

		$this->session->set_flashdata("registro",$id);
		
		redirect(base_url("usuarios"));
	}

	function get_ct(){
		$id_cst=filter_var($this->input->post('id'), FILTER_SANITIZE_STRING);
		$res=$this->construccion_md->getById($id_cst);
		$extras=$this->extras_construccion_md->GetByConstruccion($id_cst);

		echo json_encode(array("datos"=>$res,"extras"=>$extras));
	}

	function checkSes(){//recibe el id a revisar en la sesión
		$id=$this->session->userdata("id");
		
		if($id=='0'||$id==null){
			$this->session->sess_destroy();
			redirect(base_url('login'));
				
		}
	}
}
