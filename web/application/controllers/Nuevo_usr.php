<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nuevo_usr extends CI_Controller {

	function __construct() {
        // Call the Model constructor
        parent::__construct();

        $this->load->model('users_md');

    }

	public function index()
	{	
		$footer['custom_js']=array("nv_us.js");
		$this->load->view('header');
		$this->load->view('nuevo_usr');
		$this->load->view('footer',$footer);
	}

	function registrar(){
		$nombre=filter_var($this->input->post('nombre'), FILTER_SANITIZE_STRING);
		$apellido=filter_var($this->input->post("apellido"), FILTER_SANITIZE_STRING);
		$correo=filter_var($this->input->post("correo"), FILTER_SANITIZE_EMAIL);
		$pass=SHA1($this->input->post("password"));

		$usr=$this->users_md->mailExists($correo);
		$redirect="nuevo_usr";
		if(count($usr)>0){
			$this->session->set_flashdata("error",1);
		}
		else{
			$this->users_md->insertRecord(array($nombre,$apellido,$correo,$pass));
			$this->session->set_flashdata("registro",1);
			$redirect="login";
		}
		redirect(base_url($redirect));
	}

}
