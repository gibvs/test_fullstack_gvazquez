<?php

class Users_md extends CI_Model {
	
	const tabla="Users";
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function getAll()
    {
        $query = $this->db->get(self::tabla);
        return $query->result();
    }
	 
	function isUs($usr,$pass){
        /*
        $qry = "Select * from ".self::tabla." where Email='$usr' and Password='$pass'";
		$query=$this->db->query($qry);
        return $query->result();
        //*/
        $this->db->where(array("Email"=>$usr,"Password"=>$pass));
        $res=$this->db->get(self::tabla);

        return $res->result();
    }
	
    function mailExists($usr){
        $this->db->where(array("Email"=>$usr));
        $res=$this->db->get(self::tabla);

        return $res->result();
    }
    
	function insertRecord($data)
    {
        $this->FirstName   = $data[0];
        $this->LastName   = $data[1];
        $this->Email   = $data[2];
        $this->Password   = $data[3];
		
        $this->db->insert(self::tabla,$this);
		
		return $this->db->insert_id();
		
    }
	
	function updateRecord($data,$id)
    {
        $this->FirstName   = $data[0];
        $this->LastName   = $data[1];
        $this->Email   = $data[2];
        $this->Password   = $data[3];

		$this->db->update(self::tabla, $this, array('id' => $id));
		
		return $id;
		
    }

}