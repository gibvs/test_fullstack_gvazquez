<?php

class Construccion_md extends CI_Model {
	
	const tabla="Construccion";
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function getAll()
    {
        $query = $this->db->get(self::tabla);
        return $query->result();
    }


    function getView()
    {
        $qry="Select ct.*,CONCAT(us.FirstName,' ',us.LastName) as usuario from ".self::tabla." ct inner join Users us on us.id=ct.idUsr";
        $query = $this->db->query($qry);
        return $query->result();
    }

    function getByUsr($usr){
        $this->db->where(array("IdUsr"=>$usr));
        $res=$this->db->get(self::tabla);

        return $res->result();
    }
	
	function insertRecord($data)
    {
        $this->Nombre   = $data[0];
        $this->Calle   = $data[1];
        $this->Numero   = $data[2];
        $this->Colonia   = $data[3];
        $this->Delegacion   = $data[4];
        $this->Latitud   = $data[5];
        $this->Longitud   = $data[6];
        $this->Clave   = $data[7];
        $this->IdUsr   = $data[8];
		
        $this->db->insert(self::tabla,$this);
		
		return $this->db->insert_id();
		
    }
	
	function updateRecord($data,$id)
    {
        $this->Nombre   = $data[0];
        $this->Calle   = $data[1];
        $this->Numero   = $data[2];
        $this->Colonia   = $data[3];
        $this->Delegacion   = $data[4];
        $this->Latitud   = $data[5];
        $this->Longitud   = $data[6];
        $this->Clave   = $data[7];
        $this->IdUsr   = $data[8];

		$this->db->update(self::tabla, $this, array('id' => $id));
		
		return $id;
		
    }
	
    function getById($id)
    {
        $qry = "Select * from ".self::tabla." where id=".$id;
        $query=$this->db->query($qry);
        return $query->result();
    }


}