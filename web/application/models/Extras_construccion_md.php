<?php

class Extras_construccion_md extends CI_Model {
	
	const tabla="Extras_construccion";
	
	 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	function getAll()
    {
        $query = $this->db->get(self::tabla);
        return $query->result();
    }
	
    function getByConstruccion($cst){
        $this->db->where(array("id_construccion"=>$cst));
        $res=$this->db->get(self::tabla);

        return $res->result();
    }

    function delCt($cst){
        $this->db->where(array("id_construccion"=>$cst));
        $res=$this->db->delete(self::tabla);
    }
    
	function insertRecord($data)
    {
        $this->id_construccion   = $data[0];
        $this->caracteristica   = $data[1];
        $this->valor   = $data[2];
		
        $this->db->insert(self::tabla,$this);
		
		return $this->db->insert_id();
		
    }
	
	function updateRecord($data,$id)
    {
        $this->id_construccion   = $data[0];
        $this->caracteristica   = $data[1];
        $this->valor   = $data[2];

		$this->db->update(self::tabla, $this, array('id' => $id));
		
		return $id;
		
    }

}