$(function(){
  $("#guarda_cst").click(function(){
  	clst="PCOM-"+$("#str").val().toUpperCase()+"/"+$("#num").val();
  	$("#clave").val(clst);
	var clave=$("#clave").val();
  	console.log("La clave es: "+clave);
  	if(valClave(clave)){
		$("#const_form").submit(); 
	}
	else{
		alert("Veifique el formato de la clave");
	}
  }); 


  $("#agregar_car").click(function(){
    var car_ct=$("#extras .rw").length;
    var str="<div class='rw'><div class='col-md-6'><input type='text' name='car_"+car_ct+"' id='car_"+car_ct+"'></div><div class='col-md-6'><input type='text' name='vl_"+car_ct+"' id='vl_"+car_ct+"'></div></div>";
    $("#extras").append(str);
  }); 

  $(".detalles").click(function(event){
    console.log
    var idcs=$(event.target).data("idcst");
    $.post("usuarios/get_ct", {id:idcs})
      .done(function (data) {
        console.log(data);
        principal = JSON.parse(data);

        datos=principal.datos[0];

        console.log(datos);

        $("#nombre").val(datos.Nombre);
        $("#calle").val(datos.Calle);
        $("#numero").val(datos.Numero);
        $("#colonia").val(datos.Colonia);
        $("#delegacion").val(datos.Delegacion);
        $("#latitud").val(datos.Latitud);
        $("#longitud").val(datos.Longitud);
        $("#str").val(datos.Clave.substr(5,3));
        $("#num").val(datos.Clave.substr(9,2));
        $("#clave").val(datos.Clave);
        $("#idct").val(datos.ID);

        extras=principal.extras;

        extrastr="";
        $.each(extras,function(k,vl){
        	extrastr+="<div class='rw'><div class='col-md-6'><input type='text' name='car_"+k+"' id='car_"+k+"' value='"+vl.caracteristica+"'></div><div class='col-md-6'><input type='text' name='vl_"+k+"' id='vl_"+k+"' value='"+vl.valor+"'></div></div>";
        });

        $("#extras").html(extrastr);
                       
      });

  });


  $("#agregar").on("hidden.bs.modal", function () {

        $("#nombre").val("");
        $("#calle").val("");
        $("#numero").val("");
        $("#colonia").val("");
        $("#delegacion").val("");
        $("#latitud").val("");
        $("#longitud").val("");
        $("#clave").val("");
        $("#idct").val(0);

        $("#extras").html("");
	}); 

  $('#nombre').keydown(function (e) {
          if (e.shiftKey || e.ctrlKey || e.altKey) {
              e.preventDefault();
          } else {
              var key = e.keyCode;
              if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                  e.preventDefault();
              }
          }
      });

  	$('#str').keydown(function (e) {
          if (e.shiftKey || e.ctrlKey || e.altKey) {
              e.preventDefault();
          } else {
              var key = e.keyCode;
              if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                  e.preventDefault();
              }
          }
      });
  	$("#num").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
});

function valClave(clave){
	var isVal=true;
	clave=clave.trim();
	if(clave.substr(0,4)!="PCOM")
		isVal=false;
	if(clave.substr(4,1)!="-")
		isVal=false;
	if(clave.substr(8,1)!="/")
		isVal=false;

	console.log(clave.length);
	if(clave.length!=11)
		isVal=false;

	return isVal;
}
