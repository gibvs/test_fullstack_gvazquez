$(function(){

  $(".detalles").click(function(event){
    console.log
    var idcs=$(event.target).data("idcst");
    $.post("usuarios/get_ct", {id:idcs})
      .done(function (data) {
        console.log(data);
        principal = JSON.parse(data);

        datos=principal.datos[0];

        console.log(datos);

        $("#nombre").html(datos.Nombre);
        $("#calle").html(datos.Calle);
        $("#numero").html(datos.Numero);
        $("#colonia").html(datos.Colonia);
        $("#delegacion").html(datos.Delegacion);
        $("#latitud").html(datos.Latitud);
        $("#longitud").html(datos.Longitud);
        $("#clave").html(datos.Clave);
        $("#idct").html(datos.ID);

        extras=principal.extras;

        extrastr="";
        $.each(extras,function(k,vl){
        	extrastr+="<div class='rw'><div class='col-md-6 nom'>"+vl.caracteristica+":</div><div class='col-md-6'>"+vl.valor+"</div></div>";
        });

        $("#extras").html(extrastr);
                       
      });

  });

  $("#detalles").on("hidden.bs.modal", function () {

        $("#nombre").html("");
        $("#calle").html("");
        $("#numero").html("");
        $("#colonia").html("");
        $("#delegacion").html("");
        $("#latitud").html("");
        $("#longitud").html("");
        $("#clave").html("");
        $("#idct").html(0);

        $("#extras").html("");
    }); 
});
